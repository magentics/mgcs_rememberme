# Mgcs_RememberMe: Remember Me extension for Magento #

## Introduction ##
This is a true Remember Me extension for Magento in the most safe way I can think of. It was inspired by a [blog post at Inchoo](http://inchoo.net/ecommerce/magento/remember-me-checkbox-module-in-magento/) about a Remember Me checkbox extension. I made it more safe by implementing the 'theft detection' as described by Barry Jaspan [on his blog](http://jaspan.com/improved_persistent_login_cookie_best_practice). Besides that, the Inchoo extension does a time and resource consuming iteration over all customers in your store.

## Compatibility ##
I have only tested this extension in Magento 1.7.0.2. Most probably it will work just fine on earlier versions.

## Installation ##
Use the excellent [modman tool](https://github.com/colinmollenhour/modman).

## Missing templates ##
For now, there are no templates included. You have to add a checkbox with the name 'login[rememberme]' and a non-empty value to the login form yourself. Something like:

    :::html
    <input type="checkbox" name="login[rememberme]" value="1" id="cb-rememberme">
    <label for="cb-rememberme"><?php echo $this->__('Remember me'); ?></label>

## BEWARE ##
Although I made this extension as safe as I could think of, you should be very careful with implementing it. Passwords are safe, no doubt about it. But still someone can just open your customer's browser and do anything he wants with your customer's account.

## Future ##
* Restrict editing of at least password. The most work there is to let a customer log in that Magento thinks is already logged in. 

