<?php

class Mgcs_RememberMe_Model_Token extends Mage_Core_Model_Abstract
{

    /**
     * Init model
     */
    protected function _construct()
    {
        $this->_init('rememberme/token');
    }


    public function setRandomToken()
    {
        $this->setToken(sha1(uniqid(mt_rand(), true) . md5(uniqid(mt_rand(), true))));
        return $this;
    }


    public function setRandomSeries()
    {
        $this->setSeries(sha1(md5(uniqid(mt_rand(), true)) . uniqid(mt_rand(), true)));
        return $this;
    }


    public function setCustomer(Mage_Customer_Model_Customer $customer)
    {
        $this->setCustomerId($customer->getId());
        return $this;
    }


    public function setStore(Mage_Core_Model_Store $store)
    {
        $this->setStoreId($store->getId());
        return $this;
    }


    /**
    * Get the customer model associated with this token
    *
    * @return mixed  The customer model (empty if non existent) or FALSE if there's no customer id set
    */
    public function getCustomer()
    {
        if ($customerId = $this->getCustomerId()) {
            return Mage::getModel('customer/customer')->load($customerId);
        }
        return false;
    }


    public function loadByCookie($string)
    {
        $tokenData = Mage::helper('rememberme/cookie')->splitCookieValue($string);
        if ($tokenData) {
            $this->_getResource()->loadByCookie($this, $tokenData->getCustomerId(), $tokenData->getSeries(), $tokenData->getToken());
        }
        return $this;
    }


}
