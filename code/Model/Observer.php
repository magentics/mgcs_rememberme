<?php

class Mgcs_RememberMe_Model_Observer
{

    /**
    * Handle login action
    *
    * event: controller_action_postdispatch_customer_account_loginPost
    *
    * @param Varien_Event_Observer $observer
    * @return void
    */
    public function handleLogin(Varien_Event_Observer $observer)
    {
        if (!$this->_getHelper()->isEnabled()) {
            $this->_getCookieHelper()->deleteCookie();
            return;
        }
        $session = $observer->getEvent()->getCustomerSession();
        $controllerAction = $observer->getEvent()->getControllerAction();

        //Set or remove cookie depending on checkbox
        $login = $controllerAction->getRequest()->getPost('login');
        $session = $this->_getSession();
        if ($session->isLoggedIn() && !empty($login['rememberme'])) {
            $this->_log('Customer logged in with rememberme enabled: create a new token');
            $customer = $session->getCustomer();
            // create a new token with a new series identifier
            $token = Mage::getModel('rememberme/token')
                ->setCustomer($customer)
                ->setStore(Mage::app()->getStore())
                ->setRandomToken()
                ->setRandomSeries()
                ->save()
            ;
            $this->_log('Created a new token: #%d %s', array($token->getId(), $token->getData()));
            $session->setRememberMeTokenId($token->getId())
                ->setRememberMeToken($token->getToken());
            $this->_getCookieHelper()->setCookieFromToken($token);
        } else {
            $this->_getCookieHelper()->deleteCookie();
        }
    }


    /**
    * Check remember me cookie
    *
    * event: controller_action_predispatch
    *
    * @param Varien_Event_Observer $observer
    * @return void
    */
    public function checkRememberMe(Varien_Event_Observer $observer)
    {
        if (!$this->_getHelper()->isEnabled()) {
            return;
        }
        $session = Mage::getSingleton('customer/session');

        $cookie = $this->_getCookieHelper()->getCookieValue();

        if ($cookie > '') {
            if (!$session->isLoggedIn()) {
                $this->_log('Request: ' . Mage::helper('core/http')->getRequestUri() . ' :: ' . json_encode(Mage::app()->getRequest()->getParams()));
                $this->_log('Login using cookie %s', array($cookie));
                $token = Mage::getModel('rememberme/token')->loadByCookie($cookie);
                if ($token->getId()) {
                    $this->_log('Found token #%d: %s', array($token->getId(), $token->getData()));
                    $customer = $token->getCustomer();
                    if ($customer && $customer->getId()) {
                        // set customer as logged in
                        $this->_log('Set customer #%d as logged in', $customer->getId());
                        // now set a new token in the cookie and database
                        $token->setRandomToken()->save();

                        $session->setCustomerAsLoggedIn($customer)
                            ->setIsLoggedInFromRememberMe(true)
                            ->setRememberMeToken($token->getToken())
                            ->setRememberMeTokenId($token->getId());
                        $this->_log('Set a new random token: %s', $token->getToken());

                        $this->_getCookieHelper()->setCookieFromToken($token);
                        $this->_log('Set the cookie');
                    } else {
                        // customer doesn't exist anymore (in theory, this cannot happen because of the FK in the DB)
                        $this->_getCookieHelper()->deleteCookie();
                        $this->_log('Customer #%d does not exist: delete cookie', $token->getCustomerId());
                    }
                } else {
                    $this->_log('Could not find token');
                    // check if customer id / series combination already exists
                    $tokenData = Mage::helper('rememberme/cookie')->splitCookieValue($cookie);
                    if (!$tokenData) {
                        // token data is not valid
                        $collection = [];
                    } else {
                        $collection = Mage::getResourceModel('rememberme/token_collection')
                            ->addFieldToFilter('customer_id', $tokenData->getCustomerId())
                            ->addFieldToFilter('series', $tokenData->getSeries());
                    }

                    // If there's a token with matching customer and series, but NOT token, most probably
                    // a cookie theft has occurred. Delete all tokens from user and present a message to
                    // the customer
                    if (count($collection)) {
                        $this->_log('Found token in the same series for this customer: theft detection! Don\'t remove %d token(s)', array(count($collection)));
                        /*
                         * Just do not log in.
                         *
                         * It appears to happen sometimes that the theft detection gets triggered without any theft.
                         * Most probably this happens when multiple requests reach the server at (almost) the same time.
                         * The first request logs the customer in, the second sends the same cookie and the server
                         * cannot log in using this old cookie value.
                         *
                         * I think it's okay to just ignore this. We don't log the customer in, but leave the token. Yes,
                         * it weakens security. But I will not fix this anymore for a platform that is EOL in a few
                         * months.
                         *
                        $removedTokenDatas = [];
                        foreach ($collection as $removeToken) {
                            $removedTokenDatas[] = $removeToken->getData();
                        }
                        $this->_log('Removed tokens data: %s', array(json_encode($removedTokenDatas)));

                        foreach ($collection as $removeToken) {
                            $removeToken->delete();
                        }

                        if ($this->_getHelper()->isErrorMessageEnabled()) {
                            $this->_getSession()->addError(Mage::helper('rememberme')->__("Your 'remember me' has been turned off because we have detected suspicious activities on your account. It may have been used by someone else, but we are sure your password is safe. You can continue and log in to the site, but please first make sure that no-one used your account to place an order."));
                        }

                        $data = array(
                            'server' => Mage::app()->getRequest()->getServer(),
                            'params' => Mage::app()->getRequest()->getParams(),
                            'cookie' => $cookie,
                        );
                        $this->_log('Extra data: %s', json_encode($data));
                        */
                    } else {
                        $this->_log('Cookie %s is invalid: delete cookie', $cookie);
                        $this->_getCookieHelper()->deleteCookie();
                    }
                    $this->_getCookieHelper()->deleteCookie();
                }
            } else {
                // The customer is logged in: check if the cookie value is equal to the session value.
                // If not, a cookie has not been sent or received correctly by the browser. Sometimes,
                // this seem to happen because of clients aborting the connection before the headers
                // have been received or processed.
                $sessionTokenValue = $session->getRememberMeToken();
                $sessionTokenId = null;
                if ($sessionTokenValue) {
                    $sessionTokenId = $session->getRememberMeTokenId();
                }
                $tokenData = null;
                if ($sessionTokenId) {
                    $tokenData = Mage::helper('rememberme/cookie')->splitCookieValue($cookie);
                }
                if ($tokenData && $sessionTokenId && $sessionTokenValue && $sessionTokenValue != $tokenData->getToken()) {
                    // the values are not equal: try to set it again
                    $token = Mage::getModel('rememberme/token')->load($sessionTokenId);
                    if ($token->getId()) {
                        $this->_log('Request: ' . Mage::helper('core/http')->getRequestUri() . ' :: ' . json_encode(Mage::app()->getRequest()->getParams()));
                        $this->_log('Cookie %s is not the token set on login!', $cookie);
                        $this->_log('Reset cookie to token %s', $token->getToken());
                        $this->_getCookieHelper()->setCookieFromToken($token);
                    }
                }
            }
        }
        return;
    }


    /**
    * Handle logout action
    *
    * event: customer_logout
    *
    * @param Varien_Event_Observer $observer
    * @return void
    */
    public function handleLogout(Varien_Event_Observer $observer)
    {
        if (Mage::app()->getRequest()->getParam('skip_rememberme')) {
            // for debug purposes: add ability to logout without clearing the Remember Me cookie
            return;
        }
        $customer = $observer->getEvent()->getCustomer();
        $session = $this->_getSession();
        $session->setIsLoggedInFromRememberMe(false);

        $this->_log('Log out customer #%d, token id #%d', array($customer->getId(), $session->getRememberMeTokenId()));

        if ($tokenId = $session->getRememberMeTokenId()) {
            $token = Mage::getModel('rememberme/token')->load($tokenId);
            $this->_log('Delete token #%d: %s', array($token->getId(), json_encode($token->getData())));
            $token->delete();
        }

        $session->unsRememberMeTokenId();
        $this->_log('Delete cookie %s', array($this->_getCookieHelper()->getCookieValue()));
        $this->_getCookieHelper()->deleteCookie();
    }


    /**
     * Retrieve customer session model object
     *
     * @return Mage_Customer_Model_Session
     */
    protected function _getSession()
    {
        return Mage::getSingleton('customer/session');
    }


    /**
     * Retrieve RememberMe Helper
     *
     * @return Mgcs_RememberMe_Helper_Data
     */
    protected function _getHelper()
    {
        return Mage::helper('rememberme');
    }


    /**
     * Retrieve RememberMe Cookie Helper
     *
     * @return Mgcs_RememberMe_Helper_Cookie
     */
    protected function _getCookieHelper()
    {
        return Mage::helper('rememberme/cookie');
    }

    /**
     * Write a debug message in the log (shortcut for helper function)
     *
     * @param string $text
     * @param mixed  $arguments  vsprintf() arguments. A scalar value automatically gets casted to an array
     * @return void
     */
    protected function _log($text, $arguments = array())
    {
        $this->_getHelper()->debugLog($text, $arguments);
    }

}
