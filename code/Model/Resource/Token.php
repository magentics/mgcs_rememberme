<?php

class Mgcs_RememberMe_Model_Resource_Token extends Mage_Core_Model_Resource_Db_Abstract
{

    /**
     * Define main table
     */
    protected function _construct()
    {
        $this->_init('rememberme/token', 'token_id');
    }


    protected function _beforeSave(Mage_Core_Model_Abstract $object)
    {
        $object->setUpdatedAt(Mage::getSingleton('core/date')->gmtDate());

        if (!$object->getId() && null === $object->getCreatedAt()) {
            $object->setCreatedAt(Mage::getSingleton('core/date')->gmtDate());
        }

        return parent::_beforeSave($object);
    }


    public function loadByCookie(Mgcs_RememberMe_Model_Token $object, $customerId, $series, $token)
    {
        $read = $this->_getReadAdapter();
        if ($read && $customerId && $series && $token) {
            $select = $this->_getLoadSelect('customer_id', $customerId, $object)
                ->where('series = ?', $series)
                ->where('token = ?', $token);
            $data = $read->fetchRow($select);

            if ($data) {
                $object->setData($data);
            }
        }

        $this->_afterLoad($object);

        return $this;
    }


    public function deleteAllTokensForCustomer($customer)
    {
        if ($customer instanceof Mage_Customer_Model_Customer) {
            $customerId = $customer->getId();
        } else {
            $customerId = $customer;
        }

        $this->_getWriteAdapter()->delete($this->getMainTable(), array('customer_id = ?' => $customerId));
        return true;
    }

}