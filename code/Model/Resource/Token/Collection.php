<?php

class Mgcs_RememberMe_Model_Resource_Token_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{

    /**
     * Define resource model
     */
    protected function _construct()
    {
        parent::_construct();
        $this->_init('rememberme/token');
    }

}