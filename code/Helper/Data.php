<?php

class Mgcs_RememberMe_Helper_Data extends Mage_Core_Helper_Abstract
{

    const XML_PATH_ENABLED       = 'customer/rememberme/enabled';
    const XML_PATH_LIFETIME      = 'customer/rememberme/cookie_lifetime_days';
    const XML_PATH_DEBUG         = 'customer/rememberme/debug_enabled';
    const XML_PATH_DISPLAY_ERROR = 'customer/rememberme/error_message_enabled';

    protected $_debugSessionId;


    public function isEnabled()
    {
        return Mage::getStoreConfigFlag(self::XML_PATH_ENABLED);
    }


    public function getCookieLifetime()
    {
        return Mage::getStoreConfig(self::XML_PATH_LIFETIME);
    }

    public function isDebugEnabled()
    {
        return Mage::getStoreConfigFlag(self::XML_PATH_DEBUG);
    }

    public function isErrorMessageEnabled()
    {
        return Mage::getStoreConfigFlag(self::XML_PATH_DISPLAY_ERROR);
    }

    /**
     * Write a debug message in the log
     *
     * @param string $text
     * @param mixed  $arguments  vsprintf() arguments. A scalar value automatically gets casted to an array
     * @return void
     */
    public function debugLog($text, $arguments = array())
    {
        if (empty($this->_debugSessionId)) {
            $this->_debugSessionId = substr(md5(uniqid(rand())), 0, 5);
        }
        if ($this->isDebugEnabled()) {
            if (!is_array($arguments)) {
                $arguments = array($arguments);
            }
            foreach ($arguments as $key => $argument) {
                if (!is_scalar($argument)) {
                    if (is_object($argument) && is_callable($argument, 'toJson')) {
                        $arguments[$key] = $argument->toJson();
                    } else {
                        $arguments[$key] = json_encode($argument);
                    }
                }
            }
            $logText = @vsprintf($text, $arguments);
            if (!$logText) {
                $logText = sprintf('WRONG NUMBER OF ARGUMENTS! Text: "%s", arguments: %s', $text, json_encode($arguments));
            }
            Mage::log('[' . $this->_debugSessionId . '] ' . $logText, Zend_Log::DEBUG, 'mgcs_rememberme.log');
        }
    }

}