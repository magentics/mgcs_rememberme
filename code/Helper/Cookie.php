<?php

class Mgcs_RememberMe_Helper_Cookie
{

    /**
    * Set cookie from token
    *
    * @param Mgcs_RememberMe_Model_Token $token
    */
    public function setCookieFromToken($token)
    {
        $this->_getCookieModel()->set(
            $this->_getCookieName(),
            $this->composeCookieValue($token),
            Mage::helper('rememberme')->getCookieLifetime() * 86400
        );
    }


    public function deleteCookie()
    {
        $this->_getCookieModel()->delete($this->_getCookieName());
    }


    public function getCookieValue()
    {
        return $this->_getCookieModel()->get($this->_getCookieName());
    }


    /**
     * Retrieve Cookie Object
     *
     * @return Mage_Core_Model_Cookie
     */
    protected function _getCookieModel()
    {
        return Mage::app()->getCookie();
    }


    /**
    * Get the cookie name
    *
    * @return string
    */
    protected function _getCookieName()
    {
        return 'rememberme' . Mage::app()->getStore()->getId();
    }


    /**
    * Split cookie in customer_id, series and token
    *
    * @param string $string
    * @return Varien_Object
    */
    public function splitCookieValue($string)
    {
        if (preg_match('/([0-9]+):([0-9a-z]+):([0-9a-z]+)/i', $string, $matches)) {
            return new Varien_Object(array(
                'customer_id' => $matches[1],
                'series' => $matches[2],
                'token' => $matches[3],
            ));
        }
        return false;
    }


    public function composeCookieValue($token)
    {
        return implode(':', array($token->getCustomerId(), $token->getSeries(), $token->getToken()));
    }

}